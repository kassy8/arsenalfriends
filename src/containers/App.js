import React, {Component} from 'react';
import Familia from './Familia';
import SearchBox from '../components/SearchBox';
import ErrorBoundry from '../components/ErrorBoundry';
import Scroll from '../components/Scroll';
import {lafamilia,stables,teams} from '../familydata';
import '../App.css';



class App extends Component {
    constructor(){
        super();
        this.state = {
            fam: lafamilia,
            searchfor: ''
        }
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json() )
    .then(users => { console.log(users); /*this.setState({lafamilia: users });*/ });
    }

    onSearchChange = (ev) => {
        this.setState({searchfor: ev.target.value});
    }

    render(){
        const filteredfam= this.state.fam.filter(member=>{
            return member.name.toLowerCase().includes(this.state.searchfor.toLowerCase());
        });
        if(this.state.fam.length===0){
            return(<h1>Loading Tings</h1>);
        }else{
            return(<div className="tc">
            <h1 className="f1">La Familia de SEGA</h1>
            <SearchBox searchChange={this.onSearchChange} />
            <Scroll>
                <ErrorBoundry>
                    <Familia 
                    famdata={filteredfam} 
                    stable_names={stables} 
                    supported_teams={teams} />
                </ErrorBoundry>
            </Scroll>
            </div>);
        }
    }
}

export default App;