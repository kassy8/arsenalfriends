import React from 'react';
import Card from '../components/Card';


const get_football_ability_stats= () => {
    let overall= 0;
    let populate_stats= Array.from({length: 6}, () => {
        let s= Math.floor(Math.random() * 99);
        overall+= s;
        return s;
    });
    overall= Math.floor(overall/populate_stats.length); 
    return {
        overall: overall,
        PAC: populate_stats[0],
        SHO: populate_stats[1],
        PAS: populate_stats[2],
        DRI: populate_stats[3],
        DEF: populate_stats[4],
        PHY: populate_stats[5]
    }
}
const request_team= (playername,teams) => {
    let teamfound= false;
    while(teamfound === false){
        let t= prompt("What team does "+playername+" support?");
        teams.forEach(team => {
            if(team.names.includes(t) === true){
                teamfound= team;
            }
        });
    }
    return teamfound;
}


const Familia = ({famdata,stable_names,supported_teams}) => {
    throw new Error("oh dear!");
    const allfamily= famdata.map(member=>{
        const fm= member;
        let ts= request_team(fm.name,supported_teams);/*{
            id: 1,
            names: ["arsenal","afc","gooners"],
            badge: "Arsenal-FC"
        };*/
        
        const s= get_football_ability_stats();
        const st= stable_names;
        return <Card key={fm.id} familymember={fm} stats={s} stables={st} teamsupport={ts} />
    });
    return(<div>{allfamily}</div>);
}

export default Familia;
